package com.example.calculator.ui.formulalist

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import com.example.calculator.R
import com.example.calculator.data.db.entities.FormulaItem
import kotlinx.android.synthetic.main.dialog_add_formula_item.*

class AddFormulaItemDialog (context: Context, var addDialogListener: AddDialogListener) : AppCompatDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_add_formula_item)

        tvAdd.setOnClickListener {
            val name = etName.text.toString()

            if (name.isEmpty()) {
                Toast.makeText(context, "Please enter required information", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val item = FormulaItem(name, 0)
            addDialogListener.onAddButtonClicked(item)
            dismiss()
        }

        tvCancel.setOnClickListener{
            cancel()
        }
    }
}