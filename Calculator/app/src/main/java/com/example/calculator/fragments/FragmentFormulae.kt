package com.example.calculator.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.calculator.R
import com.example.calculator.data.db.entities.FormulaItem
import com.example.calculator.other.FormulaItemAdapter
import com.example.calculator.ui.formulalist.AddDialogListener
import com.example.calculator.ui.formulalist.AddFormulaItemDialog
import com.example.calculator.ui.formulalist.FormulaViewModel
import com.example.calculator.ui.formulalist.FormulaViewModelFactory
import kotlinx.android.synthetic.main.fragment_fragment_formulae.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

/**
 * A simple [Fragment] subclass.
 */
class FragmentFormulae : Fragment(), KodeinAware {

    override val kodein by closestKodein()
    private val factory: FormulaViewModelFactory by instance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_fragment_formulae, container, false)

        val viewModel = ViewModelProviders.of(this, factory).get(FormulaViewModel::class.java)

        val adapter = FormulaItemAdapter(listOf(), viewModel)

        view.rvFormulaItems.layoutManager = LinearLayoutManager(view.context)
        view.rvFormulaItems.adapter = adapter

        viewModel.getAllFormulaItems().observe(this, Observer {
            adapter.items = it
            adapter.notifyDataSetChanged()
        })

        view.fab.setOnClickListener{
            AddFormulaItemDialog(view.context,
                object : AddDialogListener {
                    override fun onAddButtonClicked(item: FormulaItem) {
                        viewModel.insert(item)
                    }
                }).show()
        }
        return view
    }
}