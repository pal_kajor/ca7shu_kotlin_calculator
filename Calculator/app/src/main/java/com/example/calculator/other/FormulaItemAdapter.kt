package com.example.calculator.other

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import com.example.calculator.R
import com.example.calculator.data.db.entities.FormulaItem
import com.example.calculator.ui.formulalist.FormulaViewModel
import kotlinx.android.synthetic.main.formula_item.view.*

class FormulaItemAdapter (

    var items: List<FormulaItem>,
    private val viewModel: FormulaViewModel
) : RecyclerView.Adapter<FormulaItemAdapter.FormulaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FormulaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.formula_item, parent, false)
        return FormulaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: FormulaViewHolder, position: Int) {
        val curFormulaItem = items[position]

        holder.itemView.tvFormula.text = "${curFormulaItem.name}"
        holder.itemView.tvTimesUsed.text = "${curFormulaItem.value}"

        holder.itemView.ivDelete.setOnClickListener {
            viewModel.delete(curFormulaItem)
        }

        holder.itemView.ivPlus.setOnClickListener {
            curFormulaItem.value++
            viewModel.insert(curFormulaItem)
        }

        holder.itemView.ivMinus.setOnClickListener {
            if (curFormulaItem.value > 0) {
                curFormulaItem.value--
                viewModel.insert((curFormulaItem))
            }
        }

        holder.itemView.ivEdit.setOnClickListener {
            it.hideKeyboard()
            holder.itemView.editText.clearFocus()
            if (!TextUtils.isEmpty(holder.itemView.editText.text.toString())) {
                curFormulaItem.name = holder.itemView.editText.text.toString()
                holder.itemView.editText.text = null
                viewModel.insert(curFormulaItem)
            }
        }

        holder.itemView.editText.setOnClickListener {
        }
    }

    private fun View.hideKeyboard() {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    inner class FormulaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}