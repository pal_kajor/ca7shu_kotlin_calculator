package com.example.calculator.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.calculator.data.db.entities.FormulaItem


@Database(
    entities = [FormulaItem::class],
    version =  1
)
abstract class FormulaDatabase : RoomDatabase() {

        abstract fun getFormulaDao(): FormulaDao

        companion object{
            @Volatile
            private var instance: FormulaDatabase? = null

            private val LOCK = kotlin.Any()

            operator fun invoke(context: Context) = instance
                ?: kotlin.synchronized(LOCK) {
                    instance
                        ?: createDatabase(
                            context
                        ).also {
                            instance = it
                        }
                }

            private fun createDatabase(context: Context) =
                androidx.room.Room.databaseBuilder(context.applicationContext,
                    FormulaDatabase::class.java,"FormulaDB.db").allowMainThreadQueries().build()
        }
    }
