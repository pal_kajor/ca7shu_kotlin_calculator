package com.example.calculator.ui.formulalist

import androidx.lifecycle.ViewModel
import com.example.calculator.data.db.entities.FormulaItem
import com.example.calculator.data.repositories.FormulaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FormulaViewModel(
    private val repository: FormulaRepository
) : ViewModel() {

    fun insert(item: FormulaItem) = CoroutineScope(Dispatchers.Main).launch {
        repository.insert(item)
    }

    fun delete(item: FormulaItem) = CoroutineScope(Dispatchers.Main).launch {
        repository.delete(item)
    }

    fun getAllFormulaItems() = repository.getAllFormulaItems()
}