package com.example.calculator.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "formula_items")
data class FormulaItem(
    @ColumnInfo(name = "formula")
    var name: String,
    @ColumnInfo(name = "times_used")
    var value: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}