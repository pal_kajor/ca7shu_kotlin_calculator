package com.example.calculator.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import com.example.calculator.R

class MainActivity : AppCompatActivity() {

    private lateinit var alert : AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentAdapter = PagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.about) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("ABOUT")
            builder.setIcon(R.drawable.ic_about2)
            builder.setMessage(R.string.info_message)
            builder.setPositiveButton("OK") { _, _ ->
                alert.cancel()
            }
            alert = builder.create()
            alert.show()
        }
        if (id == R.id.exit) {
            finishAffinity()
        }
        return super.onOptionsItemSelected(item)
    }
}
