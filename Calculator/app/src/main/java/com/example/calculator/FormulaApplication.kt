package com.example.calculator

import android.app.Application
import com.example.calculator.data.db.FormulaDatabase
import com.example.calculator.data.repositories.FormulaRepository
import com.example.calculator.ui.formulalist.FormulaViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class FormulaApplication: Application(), KodeinAware {

    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@FormulaApplication))
        bind() from singleton { FormulaDatabase(instance()) }
        bind () from singleton { FormulaRepository(instance()) }
        bind () from provider {
            FormulaViewModelFactory(instance()) }
    }
}