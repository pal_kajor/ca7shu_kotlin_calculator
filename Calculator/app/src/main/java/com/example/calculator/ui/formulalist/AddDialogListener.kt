package com.example.calculator.ui.formulalist

import com.example.calculator.data.db.entities.FormulaItem

interface AddDialogListener {

    fun onAddButtonClicked(item: FormulaItem)
}