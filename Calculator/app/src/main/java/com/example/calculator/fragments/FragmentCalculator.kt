package com.example.calculator.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.calculator.R
import kotlinx.android.synthetic.main.fragment_fragment_calculator.*
import kotlinx.android.synthetic.main.fragment_fragment_calculator.view.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.ArithmeticException

/**
 * A simple [Fragment] subclass.
 */
class FragmentCalculator : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_fragment_calculator, container, false)

        // Number Buttons
        view.tvOne.setOnClickListener {
            evaluateExpression("1", clear = true)
        }

        view.tvTwo.setOnClickListener {
            evaluateExpression("2", clear = true)
        }

        view.tvThree.setOnClickListener {
            evaluateExpression("3", clear = true)
        }
        view.tvFour.setOnClickListener {
            evaluateExpression("4", clear = true)
        }

        view.tvFive.setOnClickListener {
            evaluateExpression("5", clear = true)
        }

        view.tvSix.setOnClickListener {
            evaluateExpression("6", clear = true)
        }

        view.tvSeven.setOnClickListener {
            evaluateExpression("7", clear = true)
        }

        view.tvEight.setOnClickListener {
            evaluateExpression("8", clear = true)
        }

        view.tvNine.setOnClickListener {
            evaluateExpression("9", clear = true)
        }

        view.tvZero.setOnClickListener {
            evaluateExpression("0", clear = true)
        }

        // Operators
        view.tvPlus.setOnClickListener {
            if (view.tvExpression.text.isEmpty() || !view.tvExpression.text[view.tvExpression.text.lastIndex].isDigit()) {
                Unit
            }
            else {
                evaluateExpression("+", clear = true)
            }
        }

        view.tvMinus.setOnClickListener {
            if (view.tvExpression.text.isEmpty() || !view.tvExpression.text[view.tvExpression.text.lastIndex].isDigit()) {
                Unit
            }
            else {
                evaluateExpression("-", clear = true)
            }
        }

        view.tvMul.setOnClickListener {
            if (view.tvExpression.text.isEmpty() || !view.tvExpression.text[view.tvExpression.text.lastIndex].isDigit()) {
                Unit
            }
            else {
                evaluateExpression("*", clear = true)
            }
        }

        view.tvDivide.setOnClickListener {
            if (view.tvExpression.text.isEmpty() || !view.tvExpression.text[view.tvExpression.text.lastIndex].isDigit()) {
                Unit
            }
            else {
                evaluateExpression("/", clear = true)
            }
        }

        view.tvDot.setOnClickListener {
            val text = view.tvExpression.text.toString()
            var dot = false
            for (c in text) {
                if (!c.isDigit() && c != '.') dot = false
                if (c == '.' && !dot) dot = true
            }
            if (dot) {
                Unit
            }
            else {
                evaluateExpression(".", clear = true)
            }
        }

        view.tvClear.setOnClickListener {
            view.tvExpression.text = ""
            view.tvResult.text = ""
        }

        view.tvEquals.setOnClickListener {
            val text = view.tvExpression.text.toString()

            if (text.isEmpty() || !text[text.lastIndex].isDigit()) {
                Unit
            }
            else {
                try {
                    val expression = ExpressionBuilder(text).build()
                    val result = expression.evaluate()
                    val longResult = result.toLong()
                    if (result == longResult.toDouble()) {
                        view.tvResult.text = longResult.toString()
                    } else {
                        view.tvResult.text = result.toString()
                    }
                }
                catch (e : ArithmeticException) {
                    view.tvResult.text = "NaN"
                }
            }
        }

        view.tvBack.setOnClickListener {
            val text = view.tvExpression.text.toString()
            if(text.isNotEmpty()) {
                view.tvExpression.text = text.dropLast(1)
            }
            view.tvResult.text = ""
        }
        return view
    }

    // Function to calculate the expressions using expression builder library
    private fun evaluateExpression(string: String, clear: Boolean) {
        if(clear) {
            tvResult.text = ""
            tvExpression.append(string)
        } else {
            tvExpression.append(tvResult.text)
            tvExpression.append(string)
            tvResult.text = ""
        }
    }
/*
    override fun onResume() {
        super.onResume()
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
        imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
    }
*/
}
