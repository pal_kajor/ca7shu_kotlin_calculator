package com.example.calculator.ui.formulalist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.calculator.data.repositories.FormulaRepository

@Suppress("UNCHECKED_CAST")
class FormulaViewModelFactory(
    private val repository : FormulaRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FormulaViewModel(repository) as T
    }
}