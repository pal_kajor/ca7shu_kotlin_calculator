package com.example.calculator.data.repositories

import com.example.calculator.data.db.FormulaDatabase
import com.example.calculator.data.db.entities.FormulaItem

class FormulaRepository(

    private val db: FormulaDatabase) {

    suspend fun insert(item: FormulaItem) = db.getFormulaDao().insert(item)

    suspend fun delete(item: FormulaItem) = db.getFormulaDao().delete(item)

    fun getAllFormulaItems() = db.getFormulaDao().getAllFormulaItems()
}