package com.example.calculator.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.calculator.data.db.entities.FormulaItem

@Dao
interface FormulaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: FormulaItem)

    @Delete
    suspend fun delete(item: FormulaItem)

    @Query("SELECT * FROM formula_items")
    fun getAllFormulaItems() : LiveData<List<FormulaItem>>
}