package com.example.calculator.ui.formulalist


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calculator.data.db.FormulaDatabase
import com.example.calculator.data.db.entities.FormulaItem
import com.example.calculator.data.repositories.FormulaRepository
import com.example.calculator.getOrAwaitValue
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(manifest=Config.NONE)
class FormulaViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var db : FormulaDatabase
    private lateinit var repo : FormulaRepository
    private lateinit var vm : FormulaViewModel
    private lateinit var formula1 : FormulaItem
    private lateinit var formula2 : FormulaItem

    @Before
    fun setup() {
        db = FormulaDatabase(ApplicationProvider.getApplicationContext())
        repo = FormulaRepository(db)
        vm = FormulaViewModel(repo)

        formula1 = FormulaItem("(a+a) = 2a", 2)
        formula2 = FormulaItem("(a-a) = 0", 3)
    }

    @After
    fun closeDatabase() {
        db.close()
    }

    @Test
    fun insert_function_works() {

        vm.insert(formula1)
        vm.insert(formula2)

        val formulae = vm.getAllFormulaItems().getOrAwaitValue()

        assertEquals(formulae[0].name, "(a+a) = 2a")
        assertEquals(formulae[1].value, 3)
    }

    @Test
    fun delete_function_works() {

        Thread {
            vm.insert(formula1)

            vm.delete(formula1)

            val formulae = vm.getAllFormulaItems().getOrAwaitValue()
            assertEquals(formulae.size, 0)
            assertEquals(formulae.size, 1)
        }.start()
    }

    @Test
    fun getAllMemoryItems_function_works() {

        val formulae = vm.getAllFormulaItems().getOrAwaitValue()

        assertEquals(formulae.size, 2)
    }
}