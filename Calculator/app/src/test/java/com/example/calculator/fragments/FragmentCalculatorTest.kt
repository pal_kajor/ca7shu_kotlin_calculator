package com.example.calculator.fragments

import net.objecthunter.exp4j.ExpressionBuilder
import org.junit.Assert.*
import org.junit.Test

class FragmentCalculatorTest{

    @Test
    fun evaluateExpression_addition_works_properly() {

        val expression = "2.7+4.3"

        val value = ExpressionBuilder(expression).build()
        val result = value.evaluate()

        assertEquals(7.0, result, 0.001)
    }

    @Test
    fun evaluateExpression_subtraction_works_properly() {

        val expression = "11.45-4.43"

        val value = ExpressionBuilder(expression).build()
        val result = value.evaluate()

        assertEquals(7.02, result, 0.001)
    }

    @Test
    fun evaluateExpression_multiplication_works_properly() {

        val expression = "3.14*2.5"

        val value = ExpressionBuilder(expression).build()
        val result = value.evaluate()

        assertEquals(7.85, result, 0.001)
    }

    @Test
    fun evaluateExpression_division_works_properly() {

        val expression = "89/4"

        val value = ExpressionBuilder(expression).build()
        val result = value.evaluate()

        assertEquals(22.25, result, 0.001)
    }

    @Test
    fun evaluateExpression_order_of_operations_evaluated_properly() {

        val expression = "56+21/7-7*7+0.4"

        val value = ExpressionBuilder(expression).build()
        val result = value.evaluate()

        assertEquals(10.4, result, 0.001)
    }
}